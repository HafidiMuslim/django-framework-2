from django.contrib import admin
from .models import Asisten

# Register your models here.
@admin.register(Asisten)
class  AsistenAdmin(admin.ModelAdmin):
	list_display = ['nama_pekerja','kategori','pengalaman','usia','alamat_rumah','tgl_input','user']
	list_filter = ['nama_pekerja','kategori','pengalaman','user']
	search_fields = ['nama_pekerja','kategori','user']

